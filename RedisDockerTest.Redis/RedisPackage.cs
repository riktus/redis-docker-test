﻿using System;
using Newtonsoft.Json;

namespace RedisDockerTest.Redis
{
    public class RedisPackage
    {
        public string Outbox { get; private set; }

        public object Data { get; private set; }


        public RedisPackage(string outbox, object data)
        {
            Outbox = outbox;
            Data = data;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
