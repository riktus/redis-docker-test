﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisDockerTest.Redis;
using RedisDockerTest.WebApp.Orchestrators;
using StackExchange.Redis;

namespace RedisDockerTest.Controllers
{
    [ApiController]
    [Route("forecast")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ForecastOrchestrator _orchestrator;

        public WeatherForecastController()
        {
            _orchestrator = new ForecastOrchestrator();
        }

        [HttpPost]
        public ActionResult<string> Set(Guid id)
        {
            return _orchestrator.Proceed(id);
        }

        [HttpGet]
        public string Get()
        {
            return "Hello";
        }
    }
}
