﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RedisDockerTest.Redis;
using StackExchange.Redis;

namespace RedisDockerTest.WebApp.Orchestrators
{
    public class ForecastOrchestrator
    {
        private readonly ConnectionMultiplexer _redis;

        public ForecastOrchestrator()
        {
            _redis = ConnectionMultiplexer.Connect("redis");
        }

        public ActionResult<string> Proceed(Guid id)
        {
            try
            {
                Console.WriteLine("cntr:in");
                var t = new TaskCompletionSource<RedisValue>();

                Console.WriteLine("cntr:sub");
                _redis.GetSubscriber().Subscribe(id.ToString(), (channel, value) =>
                {
                    Console.WriteLine("cntr:closure");

                    _redis.GetSubscriber().Unsubscribe(channel);
                    t.SetResult(value);
                });

                Publish(id);

                t.Task.Wait();

                return t.Task.Result.ToString();
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }

        private void Publish(Guid id)
        {
            var package = new RedisPackage(id.ToString(), Guid.NewGuid().ToString()).ToString();
            _redis.GetDatabase().ListRightPush(id.ToString(), new RedisValue(package));

            Console.WriteLine("cntr:pub");
            _redis.GetSubscriber().Publish(Redis.RedisChannel.ConsoleService.ToString(), new RedisValue(id.ToString()));
        }
    }
}
