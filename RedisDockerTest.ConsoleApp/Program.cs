﻿using System;
using Newtonsoft.Json;
using RedisDockerTest.Redis;
using StackExchange.Redis;

namespace RedisDockerTest.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("console:in");
            var multiplexer = ConnectionMultiplexer.Connect("redis");

            Console.WriteLine("console:subs");
            multiplexer.GetSubscriber().Subscribe(Redis.RedisChannel.ConsoleService.ToString(), (_, value) =>
            {
                Console.WriteLine("console:inside");
                var data = multiplexer.GetDatabase().ListLeftPop(value.ToString());

                if (data.HasValue)
                    multiplexer.GetSubscriber().Publish(value.ToString(), new RedisValue("gotcha!"));
            });

            while(true)
            {

            }
        }

        private static RedisPackage Deserialize(string package)
        {
            return JsonConvert.DeserializeObject<RedisPackage>(package);
        }
    }
}
